=encoding UTF-8

=head1 NAME

perlmodstyle - стиль написания модулей на Perl

=head1 ВВЕДЕНИЕ

Этот документ пытается описать "наилучшие практики" ("best practice") 
 Perl-сообщества для написания Perl модулей. Она расширяет рекомендации, 
 найденные в L<perlstyle> , который должен быть внимательно рассмотрен 
перед чтением этого документа.

Хотя этот документ призван быть полезным для всех авторов модулей, 
в частности он направлен на авторов, которые хотят опубликовать свои модули на CPAN.

Основной упор делается на элементы стиля, которые видны для остальных пользователей
модуля, а не те части, которые будут видны только его разработчикам. 
Тем не менее, многие из руководящих принципов, представленных в настоящем документе
могут быть экстраполированы и успешно применятся для разработки внутренних (не-CPAN) модулей.

Этот документ отличается от L<perlnewmod> тем, что это руководство по стилю
 создания модулей CPAN, а не учебник их созданию. Он представляет
чеклист с которым можно сравнить, удовлетворяет ли данный модуль
 наилучшим практикам или нет, без подробнобностей того, как этого добиться.

Все рекомендации, содержащиеся в данном документе, была почерпнуты из
обширных бесед с опытными авторами и пользователями CPAN. Каждая
часть рекомендаций данных здесь является результатом предыдущих ошибок. 
Информация, содержащаяся здесь поможет вам тех же ошибок и дополнительной
работы, которая неизбежно потребуется, чтобы исправить их.

В первом разделе настоящего документа приводится подробный перечень;
в последующих разделах приводится более подробное обсуждение пунктов этого 
списка. В заключительном разделе "Общие Ловушки" ("Common Pitfalls"), описываются некоторые
самые популярные ошибки, сделанные авторами CPAN.

=head1 БЫСТРЫЙ ЧЕКЛИСТ

Для более подробной информации по каждому пункту в этом чеклисте, смотрите ниже.

=head2 Прежде чем ты начнешь

=over 4

=item *

Не изобретайте колесо (Don't re-invent the wheel)

=item *

Создайте патч, расширение или подкласс существующего модуля там, где это возможно
(Patch, extend or subclass an existing module where possible)

=item *

Делайте одну вещь и делать это хорошо

=item *

Выберите подходящее имя

=back

=head2 API - интерфейс модуля 

=over 4

=item *

API должен быть понятным среднего программиста

=item *

Простые методы для решения простых задач

=item *

Разделяйте функции по своему результату

=item *

Последовательное наименование подпрограмм или методов

=item *

Используйте именованные параметры (хэш-или hashref) при наличии более двух
параметров

=back

=head2 Стабильность

=over 4

=item *

Убедитесь, что ваш модуль работает под C<use strict> и C<-w>

=item *

Стабильные модули должны поддерживать обратную совместимость

=back

=head2 Документация

=over 4

=item *

Пишите документацию в POD

=item *

Назначения документа, сфера применения и целевые приложения
(Document purpose, scope and target applications)

=item *

Документируйте каждый публично доступный метод или подпрограмму, 
 в том числе параметры и возвращаемые значения

=item *

Приведите примеры использования в документации

=item *

Предоставьте файл README и, возможно, также заметки о выпуске(release notes), изменения(changelog) и т.д.

=item *

Обеспечить ссылки на дополнителную информацию (URL, адрес электронной почты)

=back

=head2 Соображения о релизе (Release considerations)

=over 4

=item *

Укажите зависимости в Makefile.PL или Build.PL

=item *

Указывайте версию Perl в кляузе C<use>

=item *

Включите в ваш модуль тесты

=item *

Choose a sensible and consistent version numbering scheme (X.YY is the common Perl module numbering scheme)

=item *

Increment the version number for every change, no matter how small

=item *

Package the module using "make dist"

=item *

Выберите соответствующую лицензию (GPL/Artistic будет хорошим значением по умолчанию)

=back

=head1 ПЕРЕД НАЧАЛОМ НАПИСАНИЯ МОДУЛЯ

Старайтесь не кидаться с головой в разработку своего модуля, не тратя
 некоторого времени на обдумывание в первую очередь. 
 Небольшая предусмотрительность может сэкономить вам огромное
количество усилий в дальнейшем. ( A little forethought may save you a vast amount of effort later on.)

=head2 Был ли было сделано раньше?

Возможно, вам даже не нужно писать модуль. Проверьте, где это уже
было сделано в Perl, и не изобретайте колесо, если у Вас нет на это
хорошей причины.

Хорошее места, чтобы искать уже существующие модули будет
http://search.cpan.org/ и спрашивайте на modules@perl.org

Если существующий модуль B<почти> делает то, что вы хотите, рассмотрите возможность написания
патча, подкласса, или иным образом расширьте существующий модуль
 не переписывая его.

=head2 Сделайте одну вещь и сделайте это хорошо

Рискуем констатировать очевидное , что модули предназначены для модульности.
Разработчик Perl должен иметь возможность использовать модули, что поставить вместе
строительные блоки своего применения. Тем не менее , важно , чтобы
блоки были правильной формы , и что разработчик не должен использовать
большой блок , когда все он нуждается маленьком.

Ваш модуль должен иметь четко определенную сферу , которую описывает не более чем
одно предложение. Можно ли ваш модуль разбить на семейство 
связанных модулей?

Плохой пример:

"FooBar.pm обеспечивает реализацию протокола FOO 
 в соответствии со стандартом BAR ".

Хороший пример:

"Foo.pm обеспечивает реализацию протокола Foo. Bar.pm
реализует соответствующий протокол BAR ".

Это означает, что если разработчику необходим только модуль для стандарта BAR,
он не должен для этого быть вынужден устанавливать библиотеку также и для FOO.

=head2 Какое будет имя?

Убедитесь, что вы выбрали подходящее имя для вашего модуля на ранних стадиях. Это
поможет людям находить и помнить выш ​​модуль и сделает программирование
 с вашим модулем более интуитивным.

При выборе названия вашего модуля , необходимо учитывать следующее:

=over 4

=item *

Будьте описательными ( т.е. точно описывайте цель модуля ) .

=item * 

Согласовывайте имя с существующими модулями.

=item *

Имя отражает функциональность модуля , а не его реализацию.

=item *

Старайтесь не начинать новую иерархию верхнего уровня , особенно если подходящая
иерархия уже существует в соответствии с которой вы могли бы поместить ваш модуль.

=back

Вы должны связаться с modules@perl.org, чтобы спросить их о вашем имени
перед публикацией модуля. Вы должны также попытаться спросить людей, которые
уже знакомы с областью применения модуля, а также исследовать систему наименований моделей CPAN. 
Авторы подобных модулей , или модули с аналогичными характеристиками
имена , могутт быть хорошим местом для начала.

=head1 ПРОЕКТИРОВАНИЕ И НАПИСАНИЕ ВАШЕГО МОДУЛЯ

Соображения по конструкции(дизайну) модуля и его кодирования:

=head2 OO или не OO?

Ваш модуль может быть объектно-ориентированным (ОО) или нет , или же он может иметь оба этих
 интерфейса. Есть плюсы и минусы у каждого метода, которые
следует учитывать при разработке вашего API.

В I<Perl Best Practices> (copyright 2004, Published by O'Reilly Media, Inc.)
Дамиан Конвей предоставляет список критериев, котрый используется при принятии решения,
 подходит ли OO для вашей проблемы:

=over 4

=item *

Система разрабатывается большая, или, вероятно, станет большой.

=item *

Данные могут быть объединены в очевидные структуры, особенно, если
есть большое количество данных в каждом агрегате.

=item *

Различные типы совокупности данных образующие естественную иерархию,
облегчают использование наследования и полиморфизма .

=item *

У вас есть часть данных, к которым применяются много различных операций.

=item *

Вы должны выполнить те же самые общие операции со связанными типами
данных, но с небольшими изменениями в зависимости от их конкретного типа.

=item *

Вполне вероятно, что позже вам придется добавлять новые типы данных.

=item *


Типичные взаимодействия между частями данных лучше всего представить в виде
операторов.

=item *

Реализация отдельных компонентов системы, скорее всего,
 будет изменена с течением времени.

=item *

Дизайн системы уже объектно-ориентированный.

=item *

Большое количество других программистов будут использовать ваши модули.

=back

Подумайте тщательно о том, подходит ли OO  для вашего модуля.
Безпричинным результатом объектной ориентации будет сложное API, которое
 будет трудно для среднего пользователя для понимания или использования.

=head2 Проектирование вашего API

Ваши интерфейсы должны быть понятны средним Perl программистам.
Следующие рекомендации могут помочь вам определить, является ли ваш API
достаточно простым:

=over 4

=item Напишите простые процедуры, чтобы делать простые вещи.

It's better to have numerous simple routines than a few monolithic ones.
If your routine changes its behaviour significantly based on its
arguments, it's a sign that you should have two (or more) separate
routines.

=item Separate functionality from output.  

Return your results in the most generic form possible and allow the user 
to choose how to use them.  The most generic form possible is usually a
Perl data structure which can then be used to generate a text report,
HTML, XML, a database query, or whatever else your users require.

If your routine iterates through some kind of list (such as a list of
files, or records in a database) you may consider providing a callback
so that users can manipulate each element of the list in turn.
File::Find provides an example of this with its 
C<find(\&wanted, $dir)> syntax.

=item Provide sensible shortcuts and defaults.

Don't require every module user to jump through the same hoops to achieve a
simple result.  You can always include optional parameters or routines for 
more complex or non-standard behaviour.  If most of your users have to
type a few almost identical lines of code when they start using your
module, it's a sign that you should have made that behaviour a default.
Another good indicator that you should use defaults is if most of your 
users call your routines with the same arguments.

=item Naming conventions

Your naming should be consistent.  For instance, it's better to have:

	display_day();
	display_week();
	display_year();

than

	display_day();
	week_display();
	show_year();

This applies equally to method names, parameter names, and anything else
which is visible to the user (and most things that aren't!)

=item Parameter passing

Use named parameters. It's easier to use a hash like this:

    $obj->do_something(
	    name => "wibble",
	    type => "text",
	    size => 1024,
    );

... than to have a long list of unnamed parameters like this:

    $obj->do_something("wibble", "text", 1024);

While the list of arguments might work fine for one, two or even three
arguments, any more arguments become hard for the module user to
remember, and hard for the module author to manage.  If you want to add
a new parameter you will have to add it to the end of the list for
backward compatibility, and this will probably make your list order
unintuitive.  Also, if many elements may be undefined you may see the
following unattractive method calls:

    $obj->do_something(undef, undef, undef, undef, undef, undef, 1024);

Provide sensible defaults for parameters which have them.  Don't make
your users specify parameters which will almost always be the same.

The issue of whether to pass the arguments in a hash or a hashref is
largely a matter of personal style. 

The use of hash keys starting with a hyphen (C<-name>) or entirely in 
upper case (C<NAME>) is a relic of older versions of Perl in which
ordinary lower case strings were not handled correctly by the C<=E<gt>>
operator.  While some modules retain uppercase or hyphenated argument
keys for historical reasons or as a matter of personal style, most new
modules should use simple lower case keys.  Whatever you choose, be
consistent!

=back

=head2 Strictness and warnings

Your module should run successfully under the strict pragma and should
run without generating any warnings.  Your module should also handle 
taint-checking where appropriate, though this can cause difficulties in
many cases.

=head2 Backwards compatibility

Modules which are "stable" should not break backwards compatibility
without at least a long transition phase and a major change in version
number.

=head2 Error handling and messages

When your module encounters an error it should do one or more of:

=over 4

=item *

Return an undefined value.

=item *

set C<$Module::errstr> or similar (C<errstr> is a common name used by
DBI and other popular modules; if you choose something else, be sure to
document it clearly).

=item *

C<warn()> or C<carp()> a message to STDERR.  

=item *

C<croak()> only when your module absolutely cannot figure out what to
do.  (C<croak()> is a better version of C<die()> for use within 
modules, which reports its errors from the perspective of the caller.  
See L<Carp> for details of C<croak()>, C<carp()> and other useful
routines.)

=item *

As an alternative to the above, you may prefer to throw exceptions using 
the Error module.

=back

Configurable error handling can be very useful to your users.  Consider
offering a choice of levels for warning and debug messages, an option to
send messages to a separate file, a way to specify an error-handling
routine, or other such features.  Be sure to default all these options
to the commonest use.

=head1 DOCUMENTING YOUR MODULE

=head2 POD

Your module should include documentation aimed at Perl developers.
You should use Perl's "plain old documentation" (POD) for your general 
technical documentation, though you may wish to write additional
documentation (white papers, tutorials, etc) in some other format.  
You need to cover the following subjects:

=over 4

=item *

A synopsis of the common uses of the module

=item *

The purpose, scope and target applications of your module

=item *

Use of each publically accessible method or subroutine, including
parameters and return values

=item *

Examples of use

=item *

Sources of further information

=item *

A contact email address for the author/maintainer

=back

The level of detail in Perl module documentation generally goes from
less detailed to more detailed.  Your SYNOPSIS section should contain a
minimal example of use (perhaps as little as one line of code; skip the
unusual use cases or anything not needed by most users); the
DESCRIPTION should describe your module in broad terms, generally in
just a few paragraphs; more detail of the module's routines or methods,
lengthy code examples, or other in-depth material should be given in 
subsequent sections.

Ideally, someone who's slightly familiar with your module should be able
to refresh their memory without hitting "page down".  As your reader
continues through the document, they should receive a progressively
greater amount of knowledge.

The recommended order of sections in Perl module documentation is:

=over 4

=item * 

NAME

=item *

SYNOPSIS

=item *

DESCRIPTION

=item *

One or more sections or subsections giving greater detail of available 
methods and routines and any other relevant information.

=item *

BUGS/CAVEATS/etc

=item *

AUTHOR

=item *

SEE ALSO

=item *

COPYRIGHT and LICENSE

=back

Keep your documentation near the code it documents ("inline"
documentation).  Include POD for a given method right above that 
method's subroutine.  This makes it easier to keep the documentation up
to date, and avoids having to document each piece of code twice (once in
POD and once in comments).

=head2 README, INSTALL, release notes, changelogs

Your module should also include a README file describing the module and
giving pointers to further information (website, author email).  

An INSTALL file should be included, and should contain simple installation 
instructions. When using ExtUtils::MakeMaker this will usually be:

=over 4

=item perl Makefile.PL

=item make

=item make test

=item make install

=back

When using Module::Build, this will usually be:

=over 4

=item perl Build.PL

=item perl Build

=item perl Build test

=item perl Build install

=back

Release notes or changelogs should be produced for each release of your
software describing user-visible changes to your module, in terms
relevant to the user.

=head1 RELEASE CONSIDERATIONS

=head2 Version numbering

Version numbers should indicate at least major and minor releases, and
possibly sub-minor releases.  A major release is one in which most of
the functionality has changed, or in which major new functionality is
added.  A minor release is one in which a small amount of functionality
has been added or changed.  Sub-minor version numbers are usually used
for changes which do not affect functionality, such as documentation
patches.

The most common CPAN version numbering scheme looks like this:

    1.00, 1.10, 1.11, 1.20, 1.30, 1.31, 1.32

A correct CPAN version number is a floating point number with at least 
2 digits after the decimal. You can test whether it conforms to CPAN by 
using

    perl -MExtUtils::MakeMaker -le 'print MM->parse_version(shift)' 'Foo.pm'

If you want to release a 'beta' or 'alpha' version of a module but
don't want CPAN.pm to list it as most recent use an '_' after the
regular version number followed by at least 2 digits, eg. 1.20_01. If
you do this, the following idiom is recommended:

  $VERSION = "1.12_01";
  $XS_VERSION = $VERSION; # only needed if you have XS code
  $VERSION = eval $VERSION;

With that trick MakeMaker will only read the first line and thus read
the underscore, while the perl interpreter will evaluate the $VERSION
and convert the string into a number. Later operations that treat
$VERSION as a number will then be able to do so without provoking a
warning about $VERSION not being a number.

Never release anything (even a one-word documentation patch) without
incrementing the number.  Even a one-word documentation patch should
result in a change in version at the sub-minor level.

=head2 Pre-requisites

Module authors should carefully consider whether to rely on other
modules, and which modules to rely on.

Most importantly, choose modules which are as stable as possible.  In
order of preference: 

=over 4

=item *

Core Perl modules

=item *

Stable CPAN modules

=item *

Unstable CPAN modules

=item *

Modules not available from CPAN

=back

Specify version requirements for other Perl modules in the
pre-requisites in your Makefile.PL or Build.PL.

Be sure to specify Perl version requirements both in Makefile.PL or
Build.PL and with C<require 5.6.1> or similar. See the section on
C<use VERSION> of L<perlfunc/require> for details.

=head2 Testing

All modules should be tested before distribution (using "make disttest"),
and the tests should also be available to people installing the modules 
(using "make test").  
For Module::Build you would use the C<make test> equivalent C<perl Build test>.

The importance of these tests is proportional to the alleged stability of a 
module. A module which purports to be stable or which hopes to achieve wide 
use should adhere to as strict a testing regime as possible.

Useful modules to help you write tests (with minimum impact on your 
development process or your time) include Test::Simple, Carp::Assert 
and Test::Inline.
For more sophisticated test suites there are Test::More and Test::MockObject.

=head2 Packaging

Modules should be packaged using one of the standard packaging tools.
Currently you have the choice between ExtUtils::MakeMaker and the
more platform independent Module::Build, allowing modules to be installed in a
consistent manner.
When using ExtUtils::MakeMaker, you can use "make dist" to create your
package. Tools exist to help you to build your module in a MakeMaker-friendly
style. These include ExtUtils::ModuleMaker and h2xs.  See also L<perlnewmod>.

=head2 Licensing

Make sure that your module has a license, and that the full text of it
is included in the distribution (unless it's a common one and the terms
of the license don't require you to include it).

If you don't know what license to use, dual licensing under the GPL
and Artistic licenses (the same as Perl itself) is a good idea.
See L<perlgpl> and L<perlartistic>.

=head1 COMMON PITFALLS

=head2 Reinventing the wheel

There are certain application spaces which are already very, very well
served by CPAN.  One example is templating systems, another is date and
time modules, and there are many more.  While it is a rite of passage to
write your own version of these things, please consider carefully
whether the Perl world really needs you to publish it.

=head2 Trying to do too much

Your module will be part of a developer's toolkit.  It will not, in
itself, form the B<entire> toolkit.  It's tempting to add extra features
until your code is a monolithic system rather than a set of modular
building blocks.

=head2 Inappropriate documentation

Don't fall into the trap of writing for the wrong audience.  Your
primary audience is a reasonably experienced developer with at least 
a moderate understanding of your module's application domain, who's just 
downloaded your module and wants to start using it as quickly as possible.

Tutorials, end-user documentation, research papers, FAQs etc are not 
appropriate in a module's main documentation.  If you really want to 
write these, include them as sub-documents such as C<My::Module::Tutorial> or
C<My::Module::FAQ> and provide a link in the SEE ALSO section of the
main documentation.  

=head1 SEE ALSO

=over 4

=item L<perlstyle>

General Perl style guide

=item L<perlnewmod>

How to create a new module

=item L<perlpod>

POD documentation

=item L<podchecker>

Verifies your POD's correctness

=item Packaging Tools

L<ExtUtils::MakeMaker>, L<Module::Build>

=item Testing tools

L<Test::Simple>, L<Test::Inline>, L<Carp::Assert>, L<Test::More>, L<Test::MockObject>

=item http://pause.perl.org/

Perl Authors Upload Server.  Contains links to information for module
authors.

=item Any good book on software engineering

=back

=head1 AUTHOR

Kirrily "Skud" Robert <skud@cpan.org>

=head1 ПЕРЕВОДЧИКИ
 
=over
 
=item * Николай Мишин C<< <mi@ya.ru> >>
 
=back
